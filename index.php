<?php if(in_array($_SERVER['SERVER_NAME'], array('michaelmafort.com','www.michaelmafort.com'))) : ?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Michael's test environment</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script-->
    <style>
      h1{color:#333 !important;}
      h4{color:#999 !important;}
    </style>
  </head>
  <body>
    <div class="container">
      <div class="jumbotron">
        <h1>Michael Mafort</h1>
        <h4>apenas um ambiente de teste</h4>
        <a href="http://www.michaelmafort.com.br/blog" class="btn btn-info">visite meu blog</a>
      </div>
      <div class="row">
        <?php
        $images = scandir(dirname(__FILE__) . "/images/");
        foreach($images as $image) :
          if(preg_match("/\.jpg$/", $image)) :
        ?>
        <div class="col-md-2"><div class="thumbnail"><img src="images/<?php echo $image;?>"></div></div>
        <?php
          endif;
        endforeach;
        ?>
      </div>
      <hr>
    </div>
  </body>
</html>
<?php else : ?>
Nothing to show here.
<?php endif; ?>